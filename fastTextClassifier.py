import mysql.connector
import os
import pickle
import random
import re
import fasttext

import pandas as pd

from bs4 import BeautifulSoup

from nltk.stem import WordNetLemmatizer
from sklearn.metrics import precision_recall_fscore_support, classification_report


def createModel(trainfile):
    wordEmbModel = ''
    hyper_params = {"lr": 0.25, "epoch": 25, "wordNgrams": 2, "bucket": 20000, "dim": 300, "thread": 2,"loss": 'softmax',"pretrainedVectors": wordEmbModel}
    model = fasttext.train_supervised(input=trainfile, **hyper_params)
    model.save_model(modelName)
    print("Model trained with the hyperparameter \n {}".format(hyper_params))


def dbConnection():
    mydb = mysql.connector.connect(
        host="",
        user="",
        passwd="",
        database=""
    )
    mycursor = mydb.cursor()
    mydb.autocommit = True
    return mydb, mycursor


def evaluateModel(sentences_with_labels, labels):
    tp = {}
    tn = {}
    fp = {}
    fn = {}
    flat_true_labels = []
    flat_predictions = []

    for s in sentences_with_labels.keys():
        actuallabels = sentences_with_labels[s][0]
        predictedlabels = sentences_with_labels[s][1]
        flat_true_labels.append(actuallabels)
        flat_predictions.append(predictedlabels)

    classif_rep = classification_report(flat_true_labels, flat_predictions, target_names=labels)
    print(classif_rep)
    microres = precision_recall_fscore_support(flat_true_labels, flat_predictions, average='micro')
    print("microres: ", microres)


# close database connection
def finish(mydb, mycursor):
    mycursor.close()
    mydb.close()


def getDevData():
    mydb, mycursor = dbConnection()
    dev = []
    devids = {}
    sql = "SELECT * FROM AGNews_sentEmbeddings WHERE datasetType = 'test';"
    mycursor.execute(sql)
    res = mycursor.fetchall()

    for row in res:
        newsId = row[0]
        text = row[1]
        label = row[2]
        dev.append([text, label])
        devids[text.strip()] = newsId

    finish(mydb, mycursor)
    return dev, devids


def getPredictions(testFile, testids):
    model = fasttext.load_model(modelName)
    lines = []

    with open(testFile, 'r') as f:
        line = f.readline()
        lines.append(line)

        while line:
            line = f.readline()
            if line != '':
                lines.append(line)

    sentences_with_labels = {}

    for sent_label in lines:
        sent = re.sub('__label__\d+', '', sent_label)
        sent = sent.replace('\n', '')
        actuallabel = re.findall('__label__\d+', sent_label, re.S)
        actuallabel = int(
            str(actuallabel).replace('__label__', '').replace("[", "").replace("]", "").replace("'", "").strip())
        predictions_per_sent = model.predict(sent, k=1)
        predicted_label = int(str(predictions_per_sent[0]).replace("('__label__", "").replace("',)", "").strip())
        sentences_with_labels[sent.strip()] = [actuallabel, predicted_label]

    return sentences_with_labels


def getTest():
    mydb, mycursor = dbConnection()
    test = []
    testids = {}
    sql = "SELECT * FROM AGNews_sentEmbeddings WHERE datasetType = 'test';"
    mycursor.execute(sql)
    res = mycursor.fetchall()

    for row in res:
        newsId = row[0]
        text = row[1]
        label = int(row[2]) - 1
        test.append([text, label])

    return test, testids


def getTrainData(categ):
    mydb, mycursor = dbConnection()
    train = []
    trainids = []
    sql = "SELECT * FROM balancedData WHERE dataset = 'agnews' AND "+categ+"= 'yes';"
    mycursor.execute(sql)
    res = mycursor.fetchall()
    finish(mydb, mycursor)

    for row in res:
        newsId = row[0]
        text = row[1]
        label = int(row[2])
        train.append([text, label])
        trainids.append([newsId, text])

    return train, trainids


def prepData(prep, fileN):
    prep_data = []

    for el in prep:
        sent = el[0]
        label = "__label__"+str(el[1])
        ftData = label + " " + sent + " \n"
        prep_data.append(ftData)

    txt_fastext = open(fileN, "w")
    txt_fastext.writelines(prep_data)


def main():
    trainfile = ''
    testfile = ''
    balanced = ['categ2','categ5','categ10', 'categ20', 'categ50']
    #random = ['200categ', '500categ', '1000categ']

    for balance in balanced:
    #for rand in random:
        train, trainids = getTrainData(balance)
        prepData(train, trainfile)

    test, testids = getTest()
    balancedFiles = ['categ2','categ10', 'categ20', 'categ50']

    for i in balancedFiles:
        createModel(trainfile)
        sentences_with_labels = getPredictions(testfile, testids)
        evaluateModel(sentences_with_labels,labels)


   
main()
