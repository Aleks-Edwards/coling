import pickle
import re
import mysql.connector

import pandas as pd
import numpy as np

from emoji import demojize
from sklearn import svm

from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report, precision_recall_fscore_support
from sklearn.naive_bayes import GaussianNB, ComplementNB
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import TweetTokenizer

tokenizer = TweetTokenizer()


def createModel(vector_train, y_train):
    model = LogisticRegression(random_state=0, solver='lbfgs',max_iter = 1000)
    model.fit(vector_train.toarray(), y_train[1])
    filename = ''
    pickle.dump(model, open(filename, 'wb'))


def dbConnection():
    mydb = mysql.connector.connect(
        host="",
        user="",
        passwd="",
        database=""
    )
    mycursor = mydb.cursor()
    mydb.autocommit = True
    return mydb, mycursor


def evaluate(y_test, y_predict, X_test,labelList):
    classif_rep = classification_report(y_test[1].to_numpy(), y_predict, target_names=labelList)
    print(classif_rep)
    title = classif_rep.split()[:3]
    pos_label = classif_rep.split()[5:8]
    neg_label = classif_rep.split()[10:13]
    neu_label = classif_rep.split()[15:18]
    macro_avg = classif_rep.split()[24:27]
    return pos_label,neg_label,neu_label,macro_avg


# close database connection
def finish(mydb, mycursor):
    mycursor.close()
    mydb.close()


def getVectorsX(dataSetType):
    x_values = {}
    mydb, mycursor = dbConnection()

    if dataSetType == 'test':
        sql = "SELECT * FROM SemEval2016_dataset WHERE datasetType = 'test';"

    else:
        sql = "SELECT * FROM balancedData WHERE dataset = 'semeval2016'AND categ2 = 'yes';"

    mycursor.execute(sql)
    result = mycursor.fetchall()
    finish(mydb, mycursor)

    for row in result:
        tID = row[0]
        tweet = row[1]
        x_values[tID] = tweet

    df_X = pd.DataFrame.from_dict(x_values.items())
    return df_X

def getVectorsY(dataSetType):
    y_values = {}
    mydb, mycursor = dbConnection()

    if dataSetType == 'test':
        sql = "SELECT * FROM SemEval2016_dataset WHERE datasetType = 'test';"
    else:
        sql = "SELECT * FROM balancedData WHERE dataset = 'semeval2016' and categ2 = 'yes';"

    mycursor.execute(sql)
    result = mycursor.fetchall()
    finish(mydb,mycursor)

    for row in result:
        tID = row[0]
        label = row[1]
        y_values[tID] = label

    df_y = pd.DataFrame.from_dict(y_values.items())
    return df_y


def predictModel(y_test,vector_test):
    modelName = ''
    model = pickle.load(open(modelName, 'rb'))
    y_predict = model.predict(vector_test)
    #zipped = zip(y_test[0], y_test[1], y_predict)
    zipped = zip(y_test[0], y_test[1], y_predict)
    return y_predict, zipped


def TFIDFVectors(X_df, df_X_train, df_X_test):
    vectorizer = TfidfVectorizer(sublinear_tf=True, use_idf=True, max_features=500, stop_words="english")
    X = vectorizer.fit_transform(X_df[1])
    vector_train = vectorizer.transform(df_X_train[1])
    vector_test = vectorizer.transform(df_X_test[1])
    feat_names = vectorizer.get_feature_names()
    return vector_train, vector_test, feat_names


def main():
    df_X_train = getVectorsX('train')
    df_y_train = getVectorsY('train')
    df_X_test = getVectorsX('test')
    df_y_test = getVectorsY('test')
    X_df = pd.concat([df_X_train, df_X_test])
    vector_train, vector_test, feat_names = TFIDFVectors(X_df, df_X_train, df_X_test)
    createModel(vector_train, df_y_train)
    y_predict, zipped_1 = predictModel(df_y_train, vector_test.toarray())
    pos_label,neg_label,neu_label,macro_avg = evaluate(df_y_test, y_predict,  df_X_test,labelList)
    microres = precision_recall_fscore_support(df_y_test[1],y_predict, average='micro')
    print(microres)



main()
