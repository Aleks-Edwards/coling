import fasttext

def createModel(readF,outModel):
    model = fasttext.train_unsupervised(readF, dim=300, wordNgrams=2)
    model.save_model(outModel)


def loadModel(outModel):
    return fasttext.load_model(outModel)


def obtainVecFile(trainedModel,vecModel):
    words = trainedModel.get_words()
    print("words: ",words)
    with open(vecModel, 'w') as file_out:
        file_out.write(str(len(words)) + " " + str(trainedModel.get_dimension()) + "\n")

        # line by line, you append vectors to VEC file
        for w in words:
            v = trainedModel.get_word_vector(w)
            #print("v: ",v)
            vstr = ""
            for vi in v:
                vstr += " " + str(vi)
            try:
                file_out.write(w + vstr + '\n')
            except:
                pass


def main():
    txtVocab = ''
    modelName = ''
    vecFile = ''
    createModel(txtVocab,modelName)
    trainedModel = loadModel(trainedModel)
    obtainVecFile(trainedModel,vecFile)
 

main()
